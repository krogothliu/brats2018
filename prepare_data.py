# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 13:57:44 2018

@author: cliu1
"""

import nibabel as nib
import os
import numpy
import gc

def run_folder(t1_data,t1ce_data,t2_data,flair_data,label_data,t1_files,t1ce_files,t2_files,flair_files,label_files,rootdir):
    for root,subFolders,files in os.walk(rootdir):
        for file in files:
            filename, file_extension = os.path.splitext(file)
            if file_extension == '.gz':
                img_file = root+'\\'+ file  
                img = nib.nifti1.load(img_file)
                data = img.get_fdata()
                if "_t2" in filename:
                    t2_data.append(data)
                    t2_files.append(file)
                elif "_flair" in filename:
                    flair_data.append(data)
                    flair_files.append(file)
                elif "_seg" in filename:
                    label_data.append(data)
                    label_files.append(file)
                elif "_t1ce" in filename:
                    t1ce_data.append(data)
                    t1ce_files.append(file)
                elif "_t1" in filename:
                    t1_data.append(data)
                    t1_files.append(file)
                else:
                    continue
                
    return [t1_data,t1ce_data,t2_data,flair_data,label_data,t1_files,t1ce_files,t2_files,flair_files,label_files]

def run_folder2(img_data,img_files,rootdir,post_fix):
    for root,subFolders,files in os.walk(rootdir):
        for file in files:
            filename, file_extension = os.path.splitext(file)
            if file_extension == '.gz':
                corename, core_extension = os.path.splitext(filename)
                if corename[-len(post_fix):]==post_fix:
                    img_file = root+'\\'+ file  
                    img = nib.nifti1.load(img_file)
                    data = img.get_fdata()
                    if post_fix in filename:
                        img_data.append(data)
                        img_files.append(file)
                    else:
                        continue
                
    return [img_data,img_files]

def run(rootdir,outputdir):    
    t1_data = []
    t1ce_data = []
    t2_data = []
    label_data = []
    flair_data = []
    t1_files = []
    t1ce_files = []
    t2_files = []
    flair_files = []
    label_files = []
    
    for root,subFolders,files in os.walk(rootdir):
        for folder in subFolders:
            print(folder)
            [t1_data,t1ce_data,t2_data,flair_data,label_data,t1_files,t1ce_files,t2_files,flair_files,label_files] = run_folder(t1_data,t1ce_data,t2_data,flair_data,label_data,t1_files,t1ce_files,t2_files,flair_files,label_files,os.path.join(root,folder))
            
    numpy.save(outputdir + '/t1.npy', numpy.asarray(t1_data,dtype='float32'))
    numpy.save(outputdir + '/t1ce.npy', numpy.asarray(t1ce_data,dtype='float32'))
    numpy.save(outputdir + '/t2.npy', numpy.asarray(t2_data,dtype='float32'))
    numpy.save(outputdir + '/flair.npy', numpy.asarray(flair_data,dtype='float32'))
    numpy.save(outputdir + '/seg.npy', numpy.asarray(label_data,dtype='int32'))
    numpy.save(outputdir + '/t1_files.npy', t1_files)
    numpy.save(outputdir + '/t1ce_files.npy', t1ce_files)
    numpy.save(outputdir + '/t2_files.npy', t2_files)
    numpy.save(outputdir + '/flair_files.npy', flair_files)
    numpy.save(outputdir + '/seg_files.npy', label_files)

def run2(rootdir,outputdir,post_fix):    
    img_data = []
    seg_files = []
    
    for root,subFolders,files in os.walk(rootdir):
        for folder in subFolders:
            print(folder)
            [img_data,seg_files] = run_folder2(img_data,seg_files,os.path.join(root,folder),post_fix)
            
    numpy.save(outputdir + '/'+post_fix+'.npy', numpy.asarray(img_data,dtype='float32'))
    numpy.save(outputdir + '/'+post_fix+'_files.npy', seg_files)

# 1. remove empty slices and reduce the number of slices to 128
# 2. padd the image to 256x256x128
def trim_and_pad(input_file, output_file,top_file,dt='uint16'):
    input_img = numpy.load(input_file)
    output_img = numpy.zeros([input_img.shape[0],256,256,128],dtype=dt)
    top_array = []
    
    for s in range(0,input_img.shape[0]):
#        remove empty slices from the top
        top = input_img.shape[3]-1
        for k in range(0,input_img.shape[3]):
            tk = input_img.shape[3]-k-1
            if max(input_img[s,:,:,tk].flatten()) != 0 or tk == 127:
                top = tk
                break
    
        marx = int((256-input_img.shape[1])/2)
        mary = int((256-input_img.shape[2])/2)
        output_img[s,marx:marx+input_img.shape[1],mary:mary+input_img.shape[2],:] = input_img[s,:,:,top-127:top+1]
        top_array.append(top)
    
    numpy.save(output_file,output_img)
    numpy.save(top_file,numpy.asarray(top_array,dtype='uint16'))
    
# 1. remove empty slices in the segmentation image
# 2. padd the image to 256x256x128
def trim_and_pad_seg(input_file, output_file,top_file,dt='uint16'):
    input_img = numpy.load(input_file)
    output_img = numpy.zeros([input_img.shape[0],256,256,64],dtype=dt)
    top_array = []
    
    for s in range(0,input_img.shape[0]):
#        remove empty slices from the top
        top = input_img.shape[3]-1
        for k in range(0,input_img.shape[3]):
            tk = input_img.shape[3]-k-1
            if max(input_img[s,:,:,tk].flatten()) != 0:
                top = tk
                break
    
        marx = int((256-input_img.shape[1])/2)
        mary = int((256-input_img.shape[2])/2)
        output_img[s,marx:marx+input_img.shape[1],mary:mary+input_img.shape[2],:] = input_img[s,:,:,top-63:top+1]
        top_array.append(top)
    
    numpy.save(output_file,output_img)
    numpy.save(top_file,numpy.asarray(top_array,dtype='uint16'))
    
def trim_and_pad2(input_file, top_file, output_file, dt='uint16'):
    input_img = numpy.load(input_file)
    output_img = numpy.zeros([input_img.shape[0],256,256,128],dtype=dt)
    top_array = numpy.load(top_file)
    
    for s in range(0,input_img.shape[0]):
        top = top_array[s]
        marx = int((256-input_img.shape[1])/2)
        mary = int((256-input_img.shape[2])/2)
        output_img[s,marx:marx+input_img.shape[1],mary:mary+input_img.shape[2],:] = input_img[s,:,:,top-127:top+1]
    
    numpy.save(output_file,output_img)
    
def trim_and_pad_seg2(input_file, top_file, output_file, dt='uint16'):
    input_img = numpy.load(input_file)
    output_img = numpy.zeros([input_img.shape[0],256,256,64],dtype=dt)
    top_array = numpy.load(top_file)
    
    for s in range(0,input_img.shape[0]):
        top = top_array[s]
        marx = int((256-input_img.shape[1])/2)
        mary = int((256-input_img.shape[2])/2)
        output_img[s,marx:marx+input_img.shape[1],mary:mary+input_img.shape[2],:] = input_img[s,:,:,top-63:top+1]
    
    numpy.save(output_file,output_img)
    
def verify():
    t1 = numpy.load('e:\\brats\\t1_files.npy')
    t1ce = numpy.load('e:\\brats\\t1ce_files.npy')
    t2 = numpy.load('e:\\brats\\t2_files.npy')
    flair = numpy.load('e:\\brats\\flair_files.npy')
    seg = numpy.load('e:\\brats\\seg_files.npy')
    
    print(len(t1)==len(t1ce))
    print(len(t1)==len(t2))
    print(len(t1)==len(flair))
    print(len(t1)==len(seg))
    
    for i in range(0,len(t1)):
        pos = t1[i].rfind('_')
#        print(t1[i][0:pos])
        if t1[i][0:pos] != t2[i][0:pos] or t1[i][0:pos] != t1ce[i][0:pos] or t1[i][0:pos] != flair[i][0:pos] or t1[i][0:pos] != seg[i][0:pos]:
            print(t1)

if __name__== "__main__":
    rootdir = "Q:\\RADONC\\ResearchData\\Winston\\MRBrain\\BraTS\\validation"
    outputdir = "D:\\brats\\validation"
    run2(rootdir,outputdir,"t1")
    gc.collect()
    run2(rootdir,outputdir,"t1ce")
    gc.collect()
    run2(rootdir,outputdir,"t2")
    gc.collect()
    run2(rootdir,outputdir,"flair")
    gc.collect()
    run2(rootdir,outputdir,"seg")
    gc.collect()
#    trim_and_pad('E:\\BraTS\\t1sample.npy','E:\\BraTS\\train\\t1sample.npy','E:\\BraTS\\train\\topsample.npy','uint16')
#    trim_and_pad2('E:\\BraTS\\segsample.npy','E:\\BraTS\\train\\topsample.npy','E:\\BraTS\\train\\segsample.npy','uint16')

#    trim_and_pad('E:\\BraTS\\t1.npy','E:\\BraTS\\train\\t1.npy','E:\\BraTS\\train\\top.npy')
#    trim_and_pad2('E:\\BraTS\\t1ce.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\t1ce.npy')
#    trim_and_pad2('E:\\BraTS\\t2.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\t2.npy')
#    trim_and_pad2('E:\\BraTS\\flair.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\flair.npy')
#    trim_and_pad2('E:\\BraTS\\seg.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\seg.npy')
    
    
    trim_and_pad_seg('E:\\BraTS\\seg.npy','E:\\BraTS\\train\\seg.npy','E:\\BraTS\\train\\top.npy')
    trim_and_pad_seg2('E:\\BraTS\\t1ce.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\t1ce.npy')
    trim_and_pad_seg2('E:\\BraTS\\t2.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\t2.npy')
    trim_and_pad_seg2('E:\\BraTS\\flair.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\flair.npy')
    trim_and_pad_seg2('E:\\BraTS\\t1.npy','E:\\BraTS\\train\\top.npy','E:\\BraTS\\train\\t1.npy')
    
#    outputdir = "E:\\brats\\train"
#    t1 = numpy.load(outputdir + '/t1.npy')
#    numpy.save(outputdir + '/t1sample.npy',t1[1:10,:,:,:])
#    t1ce = numpy.load(outputdir + '/t1ce.npy')
#    numpy.save(outputdir + '/t1cesample.npy',t1ce[1:10,:,:,:])
#    t2 = numpy.load(outputdir + '/t2.npy')
#    numpy.save(outputdir + '/t2sample.npy',t2[1:10,:,:,:])
#    flair = numpy.load(outputdir + '/flair.npy')
#    numpy.save(outputdir + '/flairsample.npy',flair[1:10,:,:,:])
#    seg = numpy.load(outputdir + '/seg.npy')    
#    numpy.save(outputdir + '/segsample.npy',seg[1:10,:,:,:])
    

    
    
#    
#    print(len(t1))
#    print(len(t1ce))
#    print(len(t2))
#    print(len(flair))
#    print(len(seg))
    
    