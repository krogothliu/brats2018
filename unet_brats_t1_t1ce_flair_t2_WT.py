import os 
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import numpy as np
import tensorflow as tf
from keras.models import *
from keras.layers import Input, merge, Dropout, Conv2D, Conv3D, MaxPooling2D, MaxPooling3D, UpSampling2D, UpSampling3D, Cropping3D, BatchNormalization, Activation, add
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as keras
from keras.utils import plot_model
from data3d import *
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import tensorflow as tf
import vtk
import generate_3d_patch_pairs as gen3d
import glob
import pickle
import operator
import csv
from vis.visualization import *
import scipy
from scipy import ndimage
import alg_lib
import generate_mask_list
import time

class myUnet(object):

    def __init__(self):
        self.working_folder = 'E:/BraTS'
        self.test_folder = self.working_folder+"/test"
        self.train_folder = self.working_folder+"/train"        
        self.validation_folder = self.working_folder+"/validation"
        self.result_folder = self.working_folder+"/t1_t1ce_flair_t2_wt"
        self.predict_folder = self.test_folder+"/predict"
        self.num_channels = 1
    
    def normalize_image(self,im):
        avg = np.mean(im,dtype='float32')
        std = np.std(im,dtype='float32')
        return (im-avg)/std
    
    def load_data(self):
#        [self.imgs_test,self.imgs_train] = self.load_data_2d('t1.npy',0.8)
#        [test1,train1] = self.load_data_2d('t1ce.npy',0.8)
#        self.imgs_test = np.append(self.imgs_test,test1,axis=3)
#        self.imgs_train = np.append(self.imgs_train,train1,axis=3)
#        [test1,train1] = self.load_data_2d('t2.npy',0.8)
#        self.imgs_test = np.append(self.imgs_test,test1,axis=3)
#        self.imgs_train = np.append(self.imgs_train,train1,axis=3)
#        [test1,train1] = self.load_data_2d('flair.npy',0.8)
#        self.imgs_test = np.append(self.imgs_test,test1,axis=3)
#        self.imgs_train = np.append(self.imgs_train,train1,axis=3)
#        [self.imgs_test,self.imgs_train] = self.load_data_2d('flair.npy',0.8)
        
        self.imgs_train = self.load_data_2d(os.path.join(self.train_folder,'t1.npy'))
        self.imgs_train = np.append(self.imgs_train,self.load_data_2d(os.path.join(self.train_folder,'t1ce.npy')),axis=3)
        self.imgs_train = np.append(self.imgs_train,self.load_data_2d(os.path.join(self.train_folder,'t2.npy')),axis=3)
        self.imgs_train = np.append(self.imgs_train,self.load_data_2d(os.path.join(self.train_folder,'flair.npy')),axis=3)
#        self.imgs_validation = self.load_data_2d(os.path.join(self.validation_folder,'flair.npy'))
#        self.imgs_test = self.load_data_2d(os.path.join(self.test_folder,'flair.npy'),1)
        
        self.imgs_mask_train = self.load_data_2d(os.path.join(self.train_folder,'seg.npy'),False)
#        self.imgs_mask_validation = self.load_data_2d(os.path.join(self.validation_folder,'seg.npy'),False)
#        self.imgs_mask_test = self.load_data_2d(os.path.join(self.test_folder,'seg.npy'),1,False)
        
#        edema is 2, non-enhanced tumor is 1, enhanced tumor is 4
#        self.imgs_mask_test[self.imgs_mask_test>0] = 1
#        self.imgs_mask_validation[self.imgs_mask_validation>0] = 1
        self.imgs_mask_train[self.imgs_mask_train>0] = 1
        
#        self.imgs_mask_test[self.imgs_mask_test==2] = 0
#        self.imgs_mask_train[self.imgs_mask_train==2] = 0
#        self.imgs_mask_test[self.imgs_mask_test==4] = 1
#        self.imgs_mask_train[self.imgs_mask_train==4] = 1
        
#        self.imgs_mask_test[self.imgs_mask_test>1] = 0
#        self.imgs_mask_train[self.imgs_mask_train>1] = 0
        
#        self.img_rows = self.imgs_test.shape[1]
#        self.img_cols = self.imgs_test.shape[2]  
        self.img_rows = self.imgs_train.shape[1]
        self.img_cols = self.imgs_train.shape[2]      
        self.num_channels = 4
    
    def load_data_2d(self,img_file,normalize = True, flip = True):
        imgs = np.load(img_file)

        imgs = np.swapaxes(np.swapaxes(imgs,1,3),2,3)
            
        img_rows = imgs.shape[2]
        img_cols = imgs.shape[3]
        
        count = imgs.shape[0]*imgs.shape[1]
        imgs=np.reshape(imgs,[count,img_rows,img_cols,1])
                
        if normalize:
            imgs = imgs.astype(np.float32)
            for i in range(0,count):
#                print(np.mean(imgs_train[i,:,:,0]))
                imgs[i,:,:,0] = self.normalize_image(imgs[i,:,:,0]) 
#                self.im_show(imgs_train[i,:,:,0].astype(int))
#                print(np.mean(imgs_train[i,:,:,0]))
                
            imgs = imgs.astype(np.float16)
                
        if flip:               
            flip_imgs = imgs
            for i in range(0,count):
                flip_imgs[i,:,:,0] = np.flip(imgs[i,:,:,0],axis=0)
            return np.append(imgs,flip_imgs,axis=0)
        
        return imgs
    
    def load_data_2d_split(self,img_file_name,split,normalize = True, flip = True):
        img_file = os.path.join(self.train_folder,img_file_name)
        imgs_train = np.load(img_file)
        s = int(split*len(imgs_train))

        imgs_train = np.swapaxes(np.swapaxes(imgs_train,1,3),2,3)
    
        imgs_test = imgs_train[s:,:,:,:]            
        imgs_train = imgs_train[0:s,:,:,:]
        
        img_rows = imgs_test.shape[2]
        img_cols = imgs_test.shape[3]
        
        test_count = imgs_test.shape[0]*imgs_test.shape[1]
        train_count = imgs_train.shape[0]*imgs_train.shape[1]
        imgs_test=np.reshape(imgs_test,[test_count,img_rows,img_cols,1])
        imgs_train=np.reshape(imgs_train,[train_count,img_rows,img_cols,1])
        
        if normalize:
            for i in range(0,test_count):
                imgs_test[i,:,:,0] = self.normalize_image(imgs_test[i,:,:,0])
                imgs_train[i,:,:,0] = self.normalize_image(imgs_train[i,:,:,0])
                
        if flip:               
            flip_test = imgs_test
            flip_train = imgs_train
            for i in range(0,test_count):
                flip_test[i,:,:,0] = np.flip(imgs_test[i,:,:,0],axis=0)
                flip_train[i,:,:,0] = np.flip(imgs_train[i,:,:,0],axis=0)
            return[np.append(imgs_test,flip_test,axis=0),np.append(imgs_train,flip_train,axis=0)]
        
        return [imgs_test,imgs_train]

    def load_data_3d(self,img_file_name,label_file_name,struct_id,split):
        self.img_file = os.path.join(self.train_folder,img_file_name)
        self.label_file = os.path.join(self.train_folder,label_file_name)
        self.imgs_train = np.load(self.img_file)
        self.imgs_mask_train = np.load(self.label_file)
        self.imgs_mask_train[self.imgs_mask_train!=struct_id]=0
        s = int(split*len(self.imgs_train))
    
        self.img_rows = self.imgs_mask_train.shape[1]
        self.img_cols = self.imgs_mask_train.shape[2]        
        self.img_depth = self.imgs_mask_train.shape[3]  
        
        self.imgs_test = np.expand_dims(self.imgs_train[s:],axis=4)
        self.imgs_mask_test = np.expand_dims(self.imgs_mask_train[s:],axis=4)
            
        self.imgs_train = np.expand_dims(self.imgs_train[0:s],axis=4)
        self.imgs_mask_train = np.expand_dims(self.imgs_mask_train[0:s],axis=4)       

    def get_unet_2d(self,feature_base = 64):

        inputs = Input((self.img_rows, self.img_cols,self.num_channels))
        conv1 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv1 shape:",conv1.shape)
        conv1 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv1)
        print ("conv1 shape:",conv1.shape)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
        print("pool1 shape:",pool1.shape)

        conv2 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool1)
        print("conv2 shape:",conv2.shape)
        conv2 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv2)
        print("conv2 shape:",conv2.shape)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
        print("pool2 shape:",pool2.shape)

        conv3 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool2)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        print("conv3 shape:",conv3.shape)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
        print("pool3 shape:",pool3.shape)

        conv4 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool3)
        conv4 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)
        drop4 = Dropout(0.5)(conv4)
        pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

        conv5 = Conv2D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool4)
        conv5 = Conv2D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv5)
        drop5 = Dropout(0.5)(conv5)

        up6 = Conv2D(feature_base*8, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(drop5))
        merge6 = merge([drop4,up6], mode = 'concat', concat_axis = 3)
        conv6 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge6)
        conv6 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)

        up7 = Conv2D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(conv6))
        merge7 = merge([conv3,up7], mode = 'concat', concat_axis = 3)
        conv7 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv2D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(conv7))
        merge8 = merge([conv2,up8], mode = 'concat', concat_axis = 3)
        conv8 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv2D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(conv8))
        merge9 = merge([conv1,up9], mode = 'concat', concat_axis = 3)
        conv9 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
        conv9 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv9 = Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv2D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-5), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    def get_unet_2d_bn(self,feature_base = 64):

        inputs = Input((self.img_rows, self.img_cols,self.num_channels))
        conv1 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv1 shape:",conv1.shape)
        conv1 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv1)
        print ("conv1 shape:",conv1.shape)        
        bn1 = BatchNormalization()(conv1)
        conv_atv1 = Activation('relu')(bn1)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv_atv1)
        print("pool1 shape:",pool1.shape)

        conv2 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool1)
        print("conv2 shape:",conv2.shape)
        conv2 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv2)
        print("conv2 shape:",conv2.shape)      
        bn2 = BatchNormalization()(conv2)
        conv_atv2 = Activation('relu')(bn2)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv_atv2)
        print("pool2 shape:",pool2.shape)

        conv3 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool2)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        print("conv3 shape:",conv3.shape)      
        bn3 = BatchNormalization()(conv3)
        conv_atv3 = Activation('relu')(bn3)
        pool3 = MaxPooling2D(pool_size=(2, 2))(conv_atv3)
        print("pool3 shape:",pool3.shape)

        conv4 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool3)
        conv4 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)      
        bn4 = BatchNormalization()(conv4)
        conv_atv4 = Activation('relu')(bn4)
        drop4 = Dropout(0.5)(conv_atv4)
        pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

        conv5 = Conv2D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool4)
        conv5 = Conv2D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv5)      
        bn5 = BatchNormalization()(conv5)
        conv_atv5 = Activation('relu')(bn5)
        drop5 = Dropout(0.5)(conv_atv5)

        up6 = Conv2D(feature_base*8, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(drop5))
        merge6 = merge([drop4,up6], mode = 'concat', concat_axis = 3)
        conv6 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge6)
        conv6 = Conv2D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)

        up7 = Conv2D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(conv6))
        merge7 = merge([conv_atv3,up7], mode = 'concat', concat_axis = 3)
        conv7 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv2D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv2D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(conv7))
        merge8 = merge([conv_atv2,up8], mode = 'concat', concat_axis = 3)
        conv8 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv2D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv2D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling2D(size = (2,2))(conv8))
        merge9 = merge([conv_atv1,up9], mode = 'concat', concat_axis = 3)
        conv9 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
        conv9 = Conv2D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv9 = Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv2D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-4), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    def get_unet_3d(self,feature_base = 16):

        inputs = Input((self.img_rows, self.img_cols, self.img_depth,self.num_channels))  

        conv1 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv1 shape:",conv1.shape)
#        conv1 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv1)
#        print ("conv1 shape:",conv1.shape)
        pool1 = MaxPooling3D(pool_size=(2, 2, 2))(conv1)
        print("pool1 shape:",pool1.shape)

        conv2 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool1)
        print("conv2 shape:",conv2.shape)
        conv2 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv2)
        print("conv2 shape:",conv2.shape)
        pool2 = MaxPooling3D(pool_size=(2, 2, 2))(conv2)
        print("pool2 shape:",pool2.shape)

        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool2)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        print("conv3 shape:",conv3.shape)
        pool3 = MaxPooling3D(pool_size=(2, 2, 2))(conv3)
        print("pool3 shape:",pool3.shape)

        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool3)
        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)
        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)
        drop4 = Dropout(0.5)(conv4)
        pool4 = MaxPooling3D(pool_size=(2, 2, 2))(drop4)
        print("pool4 shape:",pool4.shape)
        
        conv5 = Conv3D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool4)
        conv5 = Conv3D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv5)
        conv5 = Conv3D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv5)
        drop5 = Dropout(0.5)(conv5)

        up6 = Conv3D(feature_base*8, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(drop5))
        merge6 = merge([drop4,up6], mode = 'concat', concat_axis = 4)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)

        up7 = Conv3D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv6))
        merge7 = merge([conv3,up7], mode = 'concat', concat_axis = 4)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv3D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv7))
        merge8 = merge([conv2,up8], mode = 'concat', concat_axis = 4)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv3D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv8))
        merge9 = merge([conv1,up9], mode = 'concat', concat_axis = 4)
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
#        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
#        conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-5), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    
    def get_unet_3d_bn(self,feature_base = 16):

        inputs = Input((self.img_rows, self.img_cols, self.img_depth,1))  

        conv11 = Conv3D(feature_base, 3, padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv11 shape:",conv11.shape)
        bn11 = BatchNormalization()(conv11)
        conv_atv11 = Activation('relu')(bn11)
        pool11 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv11)
        print("pool11 shape:",pool11.shape)

        conv21 = Conv3D(feature_base*2, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool11)
        print("conv21 shape:",conv21.shape)        
        bn21 = BatchNormalization()(conv21)
        conv_atv21 = Activation('relu')(bn21)
        conv22 = Conv3D(feature_base*2, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv21)
        print("conv22 shape:",conv22.shape)      
        bn22 = BatchNormalization()(conv22)
        conv_atv22 = Activation('relu')(bn22)
        pool22 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv22)
        print("pool22 shape:",pool22.shape)

        conv31 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool22)
        print("conv31 shape:",conv31.shape)  
        bn31 = BatchNormalization()(conv31)
        conv_atv31 = Activation('relu')(bn31)
        conv32 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv31)
        print("conv32 shape:",conv32.shape)
        bn32 = BatchNormalization()(conv32)
        conv_atv32 = Activation('relu')(bn32)
        conv33 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv32)
        print("conv33 shape:",conv33.shape)
        bn33 = BatchNormalization()(conv33)
        conv_atv33 = Activation('relu')(bn33)
        pool33 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv33)
        print("pool33 shape:",pool33.shape)

        conv41 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool33)
        print("conv41 shape:",conv41.shape)  
        bn41 = BatchNormalization()(conv41)
        conv_atv41 = Activation('relu')(bn41)
        conv42 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv41)
        print("conv42 shape:",conv42.shape)
        bn42 = BatchNormalization()(conv42)
        conv_atv42 = Activation('relu')(bn42)
        conv43 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv42)
        print("conv43 shape:",conv43.shape)
        bn43 = BatchNormalization()(conv43)
        conv_atv43 = Activation('relu')(bn43)
        
        drop43 = Dropout(0.5)(conv_atv43)
        pool43 = MaxPooling3D(pool_size=(2, 2, 2))(drop43)
        print("pool43 shape:",pool43.shape)
        
        conv51 = Conv3D(feature_base*16, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool43)
        print("conv51 shape:",conv51.shape)  
        bn51 = BatchNormalization()(conv51)
        conv_atv51 = Activation('relu')(bn51)
        conv52 = Conv3D(feature_base*16, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv51)
        print("conv52 shape:",conv52.shape)
        bn52 = BatchNormalization()(conv52)
        conv_atv52 = Activation('relu')(bn52)
        conv53 = Conv3D(feature_base*16, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv52)
        print("conv53 shape:",conv53.shape)
        bn53 = BatchNormalization()(conv53)
        conv_atv53 = Activation('relu')(bn53)
        drop53 = Dropout(0.5)(conv_atv53)

        up6 = Conv3D(feature_base*8, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(drop53))
        merge6 = merge([drop43,up6], mode = 'concat', concat_axis = 4)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)

        up7 = Conv3D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv6))
        merge7 = merge([conv_atv33,up7], mode = 'concat', concat_axis = 4)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv3D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv7))
        merge8 = merge([conv_atv22,up8], mode = 'concat', concat_axis = 4)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv3D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv8))
        merge9 = merge([conv_atv11,up9], mode = 'concat', concat_axis = 4)
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
#        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
#        conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-5), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    def get_resnet_3d(self,feature_base = 16):

        inputs = Input((self.img_rows, self.img_cols, self.img_depth,1))  

        conv1 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv1 shape:",conv1.shape)
#        conv1 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv1)
#        print ("conv1 shape:",conv1.shape)
        pool1 = MaxPooling3D(pool_size=(2, 2, 2))(conv1)
        print("pool1 shape:",pool1.shape)

        conv2 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool1)
        print("conv2 shape:",conv2.shape)
        conv2 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv2)
        print("conv2 shape:",conv2.shape)
        pool2 = MaxPooling3D(pool_size=(2, 2, 2))(conv2)
        print("pool2 shape:",pool2.shape)

        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool2)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        print("conv3 shape:",conv3.shape)
        pool3 = MaxPooling3D(pool_size=(2, 2, 2))(conv3)
        print("pool3 shape:",pool3.shape)

        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool3)
        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)
        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)
        drop4 = Dropout(0.5)(conv4)
        pool4 = MaxPooling3D(pool_size=(2, 2, 2))(drop4)
        print("pool4 shape:",pool4.shape)
        
        conv5 = Conv3D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool4)
        conv5 = Conv3D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv5)
        conv5 = Conv3D(feature_base*16, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv5)
        drop5 = Dropout(0.5)(conv5)

        up6 = Conv3D(feature_base*8, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(drop5))
        merge6 = add([drop4,up6])
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)

        up7 = Conv3D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv6))
        merge7 = add([conv3,up7])
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv3D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv7))
        merge8 = add([conv2,up8])
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv3D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv8))
        merge9 = add([conv1,up9])
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
#        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
#        conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-5), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    def get_resnet_3d_bn(self,feature_base = 16):

        inputs = Input((self.img_rows, self.img_cols, self.img_depth,1))  

        conv11 = Conv3D(feature_base, 3, padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv11 shape:",conv11.shape)
        bn11 = BatchNormalization()(conv11)
        conv_atv11 = Activation('relu')(bn11)
        pool11 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv11)
        print("pool11 shape:",pool11.shape)

        conv21 = Conv3D(feature_base*2, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool11)
        print("conv21 shape:",conv21.shape)        
        bn21 = BatchNormalization()(conv21)
        conv_atv21 = Activation('relu')(bn21)
        conv22 = Conv3D(feature_base*2, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv21)
        print("conv22 shape:",conv22.shape)      
        bn22 = BatchNormalization()(conv22)
        conv_atv22 = Activation('relu')(bn22)
        pool22 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv22)
        print("pool22 shape:",pool22.shape)

        conv31 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool22)
        print("conv31 shape:",conv31.shape)  
        bn31 = BatchNormalization()(conv31)
        conv_atv31 = Activation('relu')(bn31)
        conv32 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv31)
        print("conv32 shape:",conv32.shape)
        bn32 = BatchNormalization()(conv32)
        conv_atv32 = Activation('relu')(bn32)
        conv33 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv32)
        print("conv33 shape:",conv33.shape)
        bn33 = BatchNormalization()(conv33)
        conv_atv33 = Activation('relu')(bn33)
        pool33 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv33)
        print("pool33 shape:",pool33.shape)

        conv41 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool33)
        print("conv41 shape:",conv41.shape)  
        bn41 = BatchNormalization()(conv41)
        conv_atv41 = Activation('relu')(bn41)
        conv42 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv41)
        print("conv42 shape:",conv42.shape)
        bn42 = BatchNormalization()(conv42)
        conv_atv42 = Activation('relu')(bn42)
        conv43 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv42)
        print("conv43 shape:",conv43.shape)
        bn43 = BatchNormalization()(conv43)
        conv_atv43 = Activation('relu')(bn43)
        
        drop43 = Dropout(0.5)(conv_atv43)
        pool43 = MaxPooling3D(pool_size=(2, 2, 2))(drop43)
        print("pool43 shape:",pool43.shape)
        
        conv51 = Conv3D(feature_base*16, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool43)
        print("conv51 shape:",conv51.shape)  
        bn51 = BatchNormalization()(conv51)
        conv_atv51 = Activation('relu')(bn51)
        conv52 = Conv3D(feature_base*16, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv51)
        print("conv52 shape:",conv52.shape)
        bn52 = BatchNormalization()(conv52)
        conv_atv52 = Activation('relu')(bn52)
        conv53 = Conv3D(feature_base*16, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv52)
        print("conv53 shape:",conv53.shape)
        bn53 = BatchNormalization()(conv53)
        conv_atv53 = Activation('relu')(bn53)
        drop53 = Dropout(0.5)(conv_atv53)

        up6 = Conv3D(feature_base*8, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(drop53))
        merge6 = add([drop43,up6])
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)
        conv6 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv6)

        up7 = Conv3D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv6))
        merge7 = add([conv_atv33,up7])
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv3D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv7))
        merge8 = add([conv_atv22,up8])
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv3D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv8))
        merge9 = add([conv_atv11,up9])
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
#        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
#        conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-5), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    def get_unet_3d2(self,feature_base = 16):

        inputs = Input((self.img_rows, self.img_cols, self.img_depth,1))  

        conv1 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv1 shape:",conv1.shape)
        conv1 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv1)
        print ("conv1 shape:",conv1.shape)
        pool1 = MaxPooling3D(pool_size=(2, 2, 2))(conv1)
        print("pool1 shape:",pool1.shape)

        conv2 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool1)
        print("conv2 shape:",conv2.shape)
        conv2 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv2)
        print("conv2 shape:",conv2.shape)
        pool2 = MaxPooling3D(pool_size=(2, 2, 2))(conv2)
        print("pool2 shape:",pool2.shape)

        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool2)
        print("conv3 shape:",conv3.shape)
        conv3 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv3)
        drop3 = Dropout(0.5)(conv3)
        print("conv3 shape:",conv3.shape)
        pool3 = MaxPooling3D(pool_size=(2, 2, 2))(drop3)
        print("pool3 shape:",pool3.shape)

        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(pool3)
        conv4 = Conv3D(feature_base*8, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv4)
        drop4 = Dropout(0.5)(conv4)

        up7 = Conv3D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(drop4))
        merge7 = merge([drop3,up7], mode = 'concat', concat_axis = 4)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv3D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv7))
        merge8 = merge([conv2,up8], mode = 'concat', concat_axis = 4)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv3D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv8))
        merge9 = merge([conv1,up9], mode = 'concat', concat_axis = 4)
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-6), loss = self.dice_loss, metrics = ['accuracy'])

        return model
    
    def get_unet_3d3(self,feature_base = 16):

        inputs = Input((self.img_rows, self.img_cols, self.img_depth,1))  

        conv11 = Conv3D(feature_base, 3, padding = 'same', kernel_initializer = 'Orthogonal')(inputs)
        print ("conv11 shape:",conv11.shape)        
#        bn11 = BatchNormalization()(conv11)
        conv_atv11 = Activation('relu')(conv11)
        conv12 = Conv3D(feature_base, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv11)
        print ("conv12 shape:",conv12.shape)      
#        bn12 = BatchNormalization()(conv12)
        conv_atv12 = Activation('relu')(conv12)
        pool12 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv12)
        print("pool12 shape:",pool12.shape)

        conv21 = Conv3D(feature_base*2, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool12)
        print("conv21 shape:",conv21.shape)        
#        bn21 = BatchNormalization()(conv21)
        conv_atv21 = Activation('relu')(conv21)
        conv22 = Conv3D(feature_base*2, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv21)
        print("conv22 shape:",conv22.shape)  
#        bn22 = BatchNormalization()(conv22)
        conv_atv22 = Activation('relu')(conv22)
        pool22 = MaxPooling3D(pool_size=(2, 2, 2))(conv_atv22)
        print("pool22 shape:",pool22.shape)

        conv31 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool22)
        print("conv31 shape:",conv31.shape)    
#        bn31 = BatchNormalization()(conv31)
        conv_atv31 = Activation('relu')(conv31)
        conv32 = Conv3D(feature_base*4, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv31)
#        bn32 = BatchNormalization()(conv32)
        conv_atv32 = Activation('relu')(conv32)
        print("conv32 shape:",conv32.shape)
        drop32 = Dropout(0.5)(conv_atv32)
        pool32 = MaxPooling3D(pool_size=(2, 2, 2))(drop32)
        print("pool32 shape:",pool32.shape)

        conv41 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(pool32) 
#        bn41 = BatchNormalization()(conv41)
        conv_atv41 = Activation('relu')(conv41)
        conv42 = Conv3D(feature_base*8, 3, padding = 'same', kernel_initializer = 'Orthogonal')(conv_atv41)
        drop42 = Dropout(0.5)(conv42)

        up7 = Conv3D(feature_base*4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(drop42))
        merge7 = merge([drop32,up7], mode = 'concat', concat_axis = 4)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge7)
        conv7 = Conv3D(feature_base*4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv7)

        up8 = Conv3D(feature_base*2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv7))
        merge8 = merge([conv_atv22,up8], mode = 'concat', concat_axis = 4)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge8)
        conv8 = Conv3D(feature_base*2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv8)

        up9 = Conv3D(feature_base, 2, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(UpSampling3D(size = (2,2,2))(conv8))
        merge9 = merge([conv_atv12,up9], mode = 'concat', concat_axis = 4)
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(merge9)
        conv9 = Conv3D(feature_base, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv9 = Conv3D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'Orthogonal')(conv9)
        conv10 = Conv3D(1, 1, activation = 'sigmoid')(conv9)

        model = Model(input = inputs, output = conv10)

        model.compile(optimizer = Adam(lr = 1e-6), loss = self.dice_loss, metrics = ['accuracy'])

        return model

    def dice_loss(self,y_true, y_pred):
        '''Just another crossentropy'''
        smooth = 1e-2
        l = tf.reduce_sum(y_true*y_true)
        r = tf.reduce_sum(y_pred*y_pred)
        return -(2*tf.reduce_sum(y_true*y_pred)+smooth)/(l+r+smooth)
    
    def np_dice_loss(self,y_true,y_pred):
        smooth = 1e-2
        l = np.sum(np.multiply(y_true,y_true))
        r = np.sum(np.multiply(y_pred,y_pred))
        return -(2*np.sum(np.multiply(y_true,y_pred))+smooth)/(l+r+smooth)
    
    def dice_loss_hard(self,y_true,y_pred):
        smooth = 1e-2
        output = tf.cast(y_pred > 0, dtype=tf.float32)
        inse = tf.reduce_sum(tf.multiply(output, y_true))
        l = tf.reduce_sum(output)
        r = tf.reduce_sum(y_true)
        hard_dice = (2. * inse + smooth) / (l + r + smooth)
        hard_dice = tf.reduce_mean(hard_dice)
        return hard_dice
    
    def np_dice_coef(self,y_true,y_pred,th = 0.5):
        output = np.copy(y_pred)
        output[output<=th] = 0
        output[output>th] = 1
        l = np.sum(np.multiply(y_true,y_true))
        r = np.sum(np.multiply(output,output))
        return (2*np.sum(np.multiply(y_true,output)))/(l+r)
    
    def train(self,start_point=None):
        print("loading data")
#        imgs_train, imgs_mask_train, imgs_test = self.load_data()
        if not os.path.exists(self.working_folder):
            os.mkdir(self.working_folder)            
            
        if not os.path.exists(self.train_folder):
            os.mkdir(self.train_folder)
            
        if not os.path.exists(self.result_folder):
            os.mkdir(self.result_folder)             
        
        self.load_data()
           
        model = None
        if start_point is None:
            model = self.get_unet_2d_bn()            
#            model = self.get_resnet_3d_bn(8)
#            model = self.get_unet_2d()
            print("got new unet")
        else:
            print('loading model from:'+start_point)
            try:
                model = load_model(start_point,custom_objects={'dice_loss': self.dice_loss})
                print("loaded unet")
            except:
                print ('loading failed!')
                return
               
        print("loading data done")
        print('Fitting model...') 
#        kfold = 5
#        sample_size = self.imgs_train.shape[0]
#        print("sample size:"+str(sample_size))
#        print("folds:"+str(kfold))
#        fold_size = int(np.floor(sample_size/kfold))
#        print("fold size:"+str(fold_size))
#        for fold in range(0,kfold):
#
#            print(fold)
#            train_imgs = np.concatenate((self.imgs_train[0:fold*fold_size,:,:,:,:],self.imgs_train[(fold+1)*fold_size:,:,:,:,:]))
#            train_masks = np.concatenate((self.imgs_mask_train[0:fold*fold_size,:,:,:,:],self.imgs_mask_train[(fold+1)*fold_size:,:,:,:,:]))
#            val_imgs = self.imgs_train[fold*fold_size:(fold+1)*fold_size,:,:,:,:]
#            val_masks = self.imgs_mask_train[fold*fold_size:(fold+1)*fold_size,:,:,:,:]
#            history = model.fit(train_imgs,train_masks, batch_size=8, nb_epoch=20, verbose=1, validation_data=(val_imgs,val_masks), shuffle=True)
#            with open(self.result_folder+'/trainHistoryDict_'+str(fold)+'.hst', 'wb') as file_pi:
#                pickle.dump(history.history, file_pi)
                
        model_checkpoint = ModelCheckpoint(self.result_folder+'/weights.{epoch:02d}-{val_loss:.2f}.hdf5', monitor='val_loss',verbose=1, save_best_only=True)
        history = model.fit(self.imgs_train, self.imgs_mask_train, batch_size=7, nb_epoch=1000, verbose=1, validation_split=0.2, shuffle=True, callbacks=[model_checkpoint])
#        history = model.fit(self.imgs_train, self.imgs_mask_train, validation_data=[self.imgs_validation, self.imgs_mask_validation],batch_size=10, nb_epoch=1000, verbose=1, shuffle=True, callbacks=[model_checkpoint])
        with open(self.result_folder+'/trainHistoryDict', 'wb') as file_pi:
            pickle.dump(history.history, file_pi)
            
    def test(self, model_file=None, reload_data = True, opt_dice=False, save_test=False, save_predict=False, save_whole_predict=False):      
        if model_file is not None:
            self.model = load_model(model_file,custom_objects={'dice_loss': self.dice_loss})
            
        if self.model is None:
            print ("please train or load a model first")
            return
                
        if not os.path.exists(self.test_folder):
            os.mkdir(self.test_folder)
            
        if not hasattr(self, 'imgs_mask_test'):
            self.load_data()
        elif reload_data:
            self.load_data()   
#        imgs_test, imgs_mask_test  = self.load_train()   
        if save_test:
            np.save(self.test_folder+'/imgs_mask_test.npy', self.imgs_mask_test)  
        print('predict test data')
        self.imgs_mask_predict = self.model.predict(self.imgs_test, batch_size=1, verbose=1)
        y_predict = self.imgs_mask_predict
        y_predict[y_predict>=0.5] = 1
        y_predict[y_predict<0.5] = 0
        
        dc = self.np_dice_coef(self.imgs_mask_test,self.imgs_mask_predict)
        print('mean dice coefficient =',dc)
         
        
        if save_predict:
            if not os.path.exists(self.predict_folder):
                os.mkdir(self.predict_folder)
            np.save(self.predict_folder+'/imgs_mask_predict.npy', self.imgs_mask_predict)
        
        return dc
        
    def imgs_show(self,imgs):
        for i in range(imgs.shape[0]):
            ndepth = imgs.shape[3]
            s = np.uint16(np.floor(ndepth/2))
            self.im_show(imgs[i,:,:,s,0])

    def im_show(self,im):
#        fig, ax = plt.subplots()
#        im = ax.imshow(im)
#        fig.colorbar(im)
#        plt.show()
             
        plt.imshow(im, cmap='gray')
        plt.show()
        
    
    def imgs_show_pair(self,imgs1,imgs2):
        for i in range(imgs1.shape[0]):
            x = alg_lib.get_largest_component(imgs2[i,:,:,:,0])
            center = ndimage.measurements.center_of_mass(x)
#            ndepth = imgs1.shape[3]
#            s = np.uint16(np.floor(ndepth/2))
            s = int(center[2])
            self.im_show(imgs1[i,:,:,s,0])
            self.im_show(x[:,:,s])
        
    def save_img(self):
        print("array to image")
        imgs = np.load(self.result_folder+'/imgs_mask_test.npy')
        for i in range(imgs.shape[0]):
            img = imgs[i]
            img = self.array_to_img(img)
            self.im_show(img)
            img.save(self.result_folder+"/%d.jpg"%(i))            
            
    def create_vtk_image_3d(self,arr,ori=[0,0,0],sp=[1,1,1],scalar_type=vtk.VTK_SHORT):
        img = vtk.vtkImageData()
        x = arr.shape[0]
        y = arr.shape[1]
        z = arr.shape[2]
        img.SetDimensions(x,y,z)
        img.SetSpacing(sp)
        img.SetOrigin(ori)
        img.AllocateScalars(scalar_type,1)
        
        for i in range(x):
            for j in range(y):
                for k in range(z):
                    img.SetScalarComponentFromDouble(i,j,k,0,arr[i,j,k])
        return img
    
    def write_meta_img(self,img,file_name):
        writer = vtk.vtkMetaImageWriter()
        writer.SetInputData(img)
        writer.SetFileName(file_name + ".mhd")
        writer.SetRAWFileName(file_name + ".raw")
        writer.Write()
        
#    find best threshold based on dice
    def opt_threshold(self,y_true,y_pred):
        opt_th = 0
        opt_dice = 0;
        for th in np.linspace(0,1,100):
            dice = self.np_dice_coef(y_true,y_pred,th)
            if dice > opt_dice:
                opt_dice = dice
                opt_th = th
        return opt_th,opt_dice
            
    def dump_imgs_3d(self,source_file_name,output_folder,scalar_type=vtk.VTK_SHORT):
        print("array to image")
        imgs = np.load(source_file_name)
        self.save_imgs_3d(imgs,output_folder,scalar_type)
        
    def dump_masks_3d(self,predict_file_name,th,output_folder,scalar_type=vtk.VTK_SHORT):
        print("array to image")
        y_predict = np.load(predict_file_name)
        y_predict[y_predict>=th] = 1
        y_predict[y_predict<th] = 0
        self.save_imgs_3d(y_predict,output_folder,scalar_type)
        
            
    def save_imgs_3d(self,imgs,output_folder,scalar_type):
        for i in range(imgs.shape[0]):
            img = imgs[i]
            if img.ndim == 4:
                img = self.create_vtk_image_3d(img[:,:,:,0],[0,0,0],[1,1,1],scalar_type)            
                self.write_meta_img(img,output_folder+"/%d"%(i))
            elif img.ndim == 3:
                img = self.create_vtk_image_3d(img[:,:,:],[0,0,0],[1,1,1],scalar_type)
                self.write_meta_img(img,output_folder+"/%d"%(i))                   
        
        
    def get_existing_results(self,result_file):
        res = []
        with open(result_file, newline='') as csvfile:
             csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
             for row in csvreader:
#                loc = row[0].find('weights')
                res.append(row[0])
        return res
        
    # test all the models in a folder and write the result in a file
    def test_all_in_folder(self,model_root,result_file):
        models = glob.glob(model_root+'/*.hdf5')
        
        result_dict = {}
        keys = []
        
        for m in models:            
            k = self.get_epoch_from_weight_file_name(m)        
            if k>=0:
                result_dict[k] = m
                keys.append(k)
                
        keys.sort()
        
        exist_res = []
        
        if os.path.isfile(result_file):
            exist_res = self.get_existing_results(result_file)
                
        for k in keys:
            model_file = result_dict[k]
            
            if model_file not in exist_res:
    #            print(k,model_file)
                self.test_cbct_individual(model_file,result_file)
#                dc = self.test(model_file,False)
#                with open(result_file, "a") as myfile:
#                    myfile.write(model_file+','+str(dc)+'\n')    
        
            
    def get_epoch_from_weight_file_name(self,l):
        s = l.find('weights.')
        e = l.find('--')
        k = -1
        if s!=-1 and e != -1 and e-s>9:
            k = int(l[s+8:e])
        return k
    
#    sort files according to epoch
    def sort_result_file(self,input_file):
        f = open(input_file,"r") #opens file with name of "test.txt"
    
        list_data = {}
        key_data = []
        for l in f:            
            k = self.get_epoch_from_weight_file_name(l)
            if k>=0:
                list_data[k] = l
                key_data.append(k)
        f.close()
        
        key_data.sort();
        
        out_f = open(input_file,"w")
        for k in key_data:
            out_f.write(list_data[k])
        out_f.close()
        
#    sort the prediction results based on dice between the truth and save to new files
    def sort_prediction(self,predict_file,truth_file=None, image_file=None):        
        y_pred = np.load(predict_file)
        
        if truth_file is not None:
            y_true = np.load(truth_file)
        else:
            y_true = self.imgs_mask_test
            def_truth_folder = self.test_folder + '/truth'
            if not os.path.exists(def_truth_folder):
                os.mkdir(def_truth_folder)
            truth_file = def_truth_folder+'/imgs_mask_test.npy'
            
        if image_file is not None:
            x = np.load(image_file)
        else:
            x = self.imgs_test
            def_image_folder = self.test_folder + '/images'
            if not os.path.exists(def_image_folder):
                os.mkdir(def_image_folder)
            image_file = def_image_folder+'/imgs_test.npy'
        
        out_truth_file = truth_file+'.sort'
        out_predict_file = predict_file+'.sort'
        out_image_file = image_file+'.sort'
        
        nsamples = y_true.shape[0]
        nrows = y_true.shape[1]
        ncols = y_true.shape[2]
        ndepth = y_true.shape[3]
        dict_data = []
        
        for i in range(0,nsamples):
            dc = self.np_dice_coef(y_true[i,:,:,:],y_pred[i,:,:,:])
            dict_data.append([dc,i])
#            print(dc)
#            self.im_show(y_true[i,:,:,32,0])
#            self.im_show(y_pred[i,:,:,32,0])
            
        sorted_data = sorted(dict_data, key=lambda tup: tup[0])
        
        out_y_true = np.ndarray((nsamples,nrows,ncols,ndepth,1), dtype=np.uint8)
        out_y_pred = np.ndarray((nsamples,nrows,ncols,ndepth,1), dtype=np.uint8)
        out_x = np.ndarray((nsamples,nrows,ncols,ndepth,1), dtype=np.float32)
        
        th = 0.5
        y_pred[y_pred<=th] = 0
        y_pred[y_pred>th] = 1
        
        sort_file = image_file+'.sort.txt'
        with open(sort_file, "w") as myfile:
            for i in range(0,nsamples):
                k = sorted_data[i][1]
                out_y_true[i,:,:,:,0] = y_true[k,:,:,:,0]
                out_y_pred[i,:,:,:,0] = y_pred[k,:,:,:,0]
                out_x[i,:,:,:,0] = x[k,:,:,:,0]
    #            self.im_show(out_y_true[i,:,:,32,0])
    #            self.im_show(out_y_pred[i,:,:,32,0])
    #            self.im_show(out_x[i,:,:,32,0])
                print(sorted_data[i][0])
                myfile.write(str(k)+'\n')
            
        np.save(out_truth_file,out_y_true)
        np.save(out_predict_file,out_y_pred)
        np.save(out_image_file,out_x)
                
                
    def visualize(self, model_file=None):      
        if model_file is not None:
            model = load_model(model_file,custom_objects={'dice_loss': self.dice_loss})                    
            for layer_idx in range(0,len(model.layers)):
                model.layers[layer_idx].get_config()
                img = visualize_activation(model, layer_idx)
                self.im_show(img[:,:,32,0])
                
    def visualize_layer(self, input_file, model_file=None, snap_folder = None):      
        if model_file is not None:
            model = load_model(model_file,custom_objects={'dice_loss': self.dice_loss})                    
            input_img = gen3d.normalize_image(np.load(input_file)[0,:,:,:])
            input_img = np.expand_dims(input_img,axis=0)
            img_aug = np.expand_dims(input_img,axis=4)
            
            if snap_folder is not None:
                if not os.path.exists(snap_folder):
                    os.mkdir(snap_folder)
                    
            for layer_idx in range(0,len(model.layers)):
                intermediate_layer_model = Model(inputs=model.input,
                outputs=model.layers[layer_idx].output)
                intermediate_output = intermediate_layer_model.predict(img_aug)
                print(intermediate_output.shape)
                tile = int(np.ceil(np.sqrt(intermediate_output.shape[4])))
                image_tile = np.zeros([tile*intermediate_output.shape[1],tile*intermediate_output.shape[2]])
                
                counter = 0
                for j in range(0,tile):
                    for k in range(0,tile):
                        if counter < intermediate_output.shape[4]:
                            img = intermediate_output[0,:,:,:,counter]
                            print(img.shape)
                            image_tile[j*img.shape[0]:(j+1)*img.shape[0],k*img.shape[1]:(k+1)*img.shape[1]] = img[:,:,int(img.shape[2]/2)]
    #                        self.im_show(img[:,:,int(img.shape[2]/2)])
                            counter = counter + 1

                name = str(img.shape[0])+'_'+str(img.shape[1])+'_'+str(img.shape[2])
                jpg_file = snap_folder + '/' + str(layer_idx) + '_' + name + '.jpg'
                scipy.misc.imsave(jpg_file, image_tile)
                
    def plot_history(self,history_folder):
        for fi in glob.glob(history_folder+'/*.hst'):
            hst = np.load(fi)
            y = hst['val_loss']
            plt.plot(range(0,len(y)),y, 'go-', label='line 1', linewidth=2)
            
    def save_figs(self):        
        fig_folder = os.path.join(self.result_folder,'figures')
        if not os.path.exists(fig_folder):
            os.mkdir(fig_folder)
            
        for i in range(0,self.imgs_test.shape[0]):
            fig_file = os.path.join(fig_folder,str(i)+'.png')
            plt.subplot(131)
            plt.imshow(self.imgs_test[i,:,:,0],cmap='gray',norm=colors.Normalize(vmin=min(self.imgs_test[i,:,:,0].flatten()), vmax=max(self.imgs_test[i,:,:,0].flatten())))
            plt.subplot(132)
            plt.imshow(self.imgs_mask_test[i,:,:,0],cmap='gray')
            plt.subplot(133)
            predict = self.imgs_mask_predict[i,:,:,0]
            predict[predict>=0.5] = 1
            predict[predict<0.5] = 0
            plt.imshow(predict,cmap='gray')
            plt.savefig(fig_file,dpi=300)

def run():
#    config = tf.ConfigProto()
#    config.gpu_options.allow_growth = True
    myunet = myUnet()
#    myunet.train('E:\\BraTS\\results_4pool_8FeatureMap\\t1ce_NC_ET\\weights.62--0.87.hdf5')
    myunet.train()
#    myunet.test_all_in_folder('E:/prostate_samples/128x128_3d/cbct_results1','E:/prostate_samples/128x128_3d/cbct_results1/cbct_test_results1.csv')
#    myunet.dump_imgs_3d('E:\\prostate_samples\\128x128_3d\\test\\adoemer1_imgs_train.npy','E:\\prostate_samples\\128x128_3d\\test\\mhd')
    #reload data only
    
    
def test():
    myunet = myUnet()
    myunet.test('E:\\BraTS\\results_4pool_8FeatureMap\\168_42_0\\flair_WT\\round1\\weights.36--0.92.hdf5',True,False,False,False,False)
#    myunet.test('E:\\BraTS\\results_4pool_8FeatureMap\\t1ce_NC\\weights.56--0.78.hdf5',True,False,False,False,False)
    myunet.save_figs()
#    myunet.test('E:/prostate_samples/128x128_3d/results4/weights.902--0.88.hdf5',True,False,False,False,True)        

def show_results(root_folder, file_name1, file_name2, file_name3):
    imgs1 = np.load(file_name1)
    imgs2 = np.load(file_name2)
    imgs3 = np.load(file_name3)
    
    for i in range(0,imgs1.shape[0]):
        fig_folder = os.path.join(root_folder,'figures')
        fig_file = os.path.join(fig_folder,str(i)+'.png')
        plt.subplot(131)
        plt.imshow(imgs1[i,:,:,0],norm=colors.Normalize(vmin=min(imgs1[i,:,:,0].flatten()), vmax=max(imgs1[i,:,:,0].flatten())))
        plt.subplot(132)
        plt.imshow(imgs2[i,:,:,0])
        plt.subplot(133)
        plt.imshow(imgs3[i,:,:,0])
        plt.savefig(fig_file)
#        gen3d.im_show(imgs1[i,:,:,0])
#        print(1)
#        gen3d.im_show(imgs2[i,:,:,0])
#        print(2)
        time.sleep(1)

if __name__ == '__main__':
#    unet = myUnet()
#    load_model('E:\\BraTS\\results_4pool_8FeatureMap\\t1ce_TC\\weights.96--0.91.hdf5',custom_objects={'dice_loss': unet.dice_loss})
    run()
#    test()
#    show_results('E:\\BraTS\\train\\seg.npy')
#    show_results('E:\\BraTS\\test\\imgs_mask_test.npy')