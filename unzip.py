# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 11:40:18 2018

@author: cliu1
"""

import gzip
import os
from subprocess import call

def unzip(input_file,output_file):    
    inF = gzip.GzipFile(input_file, 'rb')
    s = inF.read()
    inF.close()
    
    outF = open(output_file, 'wb')
    outF.write(s)
    outF.close()
    
def process_file(root,file):
    filename, file_extension = os.path.splitext(file)
    corename, core_extension = os.path.splitext(filename)
    if file_extension == '.gz':
        input_file = root+'\\'+ file         
        output_file = root+'\\'+filename  
        mhd_file = root+'\\'+corename+'.mhd'
        unzip(input_file,output_file)
        call(["C:\\Projects\\ImageTools_vc14_x64\\3d\\Release\\nifti2mhd.exe", output_file,mhd_file])

def unzip_folder(rootdir,prefix):
    for root,subFolders,files in os.walk(rootdir):
        for file in files:
            process_file(root,file)
                
                

#rootdir = "Q:\\RADONC\\ResearchData\\Winston\\MRBrain\\BraTS\\validation"
rootdir = "Q:\\RADONC\\ResearchData\\Winston\\MRBrain\\BraTS\\train_results_t1_t1ce_t2_flair\\upload"
for root,subFolders,files in os.walk(rootdir):
    for file in files:
        process_file(rootdir,file)
#    for folder in subFolders:
#        print(folder)
#        unzip_folder(os.path.join(root,folder),"mask")
